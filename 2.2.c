#include <stdio.h>
#define Pi 3.14

int main()
{
    float r,h;
    printf("Enter the radius to calculate the Area of the cylinder: ");
    scanf("%f",&r);
    printf("Enter the height to calculate the Area of the cylinder: ");
    scanf("%f",&h);

    float Area = (2*Pi*r*h) + (Pi*r*r*2);
    printf("Area of the cylinder: %.2f\n",Area);

    return 0;

}
